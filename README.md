# Summoner : A ModLoader for Caller's Bane

## What is it?
Summoner is a project to provide a modding API for Caller's Bane by Mojang. (Previously known as "Scrolls")

- [Summoner : A ModLoader for Caller's Bane](#summoner--a-modloader-for-callers-bane)
  - [What is it?](#what-is-it)
  - [Contributors](#contributors)
  - [Motivation](#motivation)
    - [Original](#original)
    - [Caller's Bane](#callers-bane)
  - [Concept](#concept)
  - [Releases](#releases)
  - [What was changed from the Concept:](#what-was-changed-from-the-concept)
  - [Developer API](#developer-api)
  - [Build instructions (needs to be updated)](#build-instructions-needs-to-be-updated)
    - [Requirements](#requirements)
    - [How to setup the build environment](#how-to-setup-the-build-environment)
  - [Future Plans](#future-plans)

## Contributors
This project has been through a lot over the last 5 years.  
Major thanks to the original author(s) and contributor(s) to the project:

Previous developers:  
Drakulix - Main Developer  
kbasten - Developer  
noHero - Developer  

Currently the project has been updated to ensure compatibility with Caller's Bane by daemuth.

##  Motivation

### Original

Why doing a ModLoader for Caller's Bane?

Caller's Bane is a game based on the Unity-Engine, which uses Mono in background to provide it's functionality easily over a wider range of platforms.
Mono/.net Assemblies are compiled into a VM-ASM similar to Java, that is interpreted by the Runtime Environment.
Making a ModLoader for Caller's Bane provides an example to do similar stuff with every other Unity-powered games (even for iPhone or Android implementations) and Mono/.net Assemblies in general.
Which is interesting to understand for myself and many more people, I assume.

Not at least Caller's Bane is a fun-making game, that deserves to have a nice community and modding always helps to turn the game into what the users expect.
Mojang should know about that from making Minecraft quite well.

### Caller's Bane
Even after several years, Caller's Bane remains a unique game and has a small but very engaged community. Now that the client has been released but there won't be any further official patches, the need for a solid modding platform is greater than ever to ensure the longevity of the project.

## Concept

The Caller's Bane ModLoader loads itself into the assembly through a little patcher utilizing Mono.Cecil, ILRepack and the execellent LinFu Framework (both also based on Mono.Cecil).
Mono.Cecil provides a way to manipulated compiled assemblies very easily, LinFu takes that to the next level and ILRepack makes injecting code even easier.

The ModLoader assembly will be used as Patcher and injected Assembly.
When run on its own it patches some basic calls into the Caller's Bane Assembly and merges itself into it to be called by Caller's Bane at Runtime.
This gives us the possibility to do more patches at runtime without need to call the patcher directly.
Instead in case any changes to the Assembly are required, it will modify itself through the injected Patcher code on its own.
This includes game patches or blocking hooks of older incompatible mods.

Mods themselves will have multiple ways to modify the Caller's Bane gaming experience.

The Caller's Bane ModLoader provides two APIs:  

The **Low-Level**-API and the **High-Level**-API.

The **High-Level-API** hides all hooks and gives some basic functionality through a standard API, that is guaranteed to be available on all game versions, but may change with new versions of the API.

The **Low-Level-API** provides a way to directly patch Caller's Bane like the ModLoader does, through a simplified API (specialized on Caller's Bane) for Mono.Cecil.

The **Low-Level-API** calls are likely to break on future game versions, as we are not making the game itself. However the ModLoader API is doing checks in background to prevent crashes. It acts as additional security layer to detect outdated or simply broken mods to ensure the game is never broken to an unplayable state through bad code.
However Mods are able to do anything through the Low-Level API, which means they can access the user account directly and modify the GUI in any way they want.

Although this is very insecure, it is clear that no High-Level-API could ever provide the possibilities, that the Low-Level-API gives to modders.
Even if we would not provide this API, developers would be able to use Mono.Cecil themselves to get access to certain functions.
So it is better to control those mods through this little layer for compatibility reasons, instead of giving all responsibility to the mod developers.
To ensure safety for the user we try to provide a trusted-platform for the Caller's Bane ModLoader provided by...

## Releases

At [Scrollsguide](https://scrollsguide.com/summoner)!

Any releases will be provided through [scrollsguide.com](https://scrollsguide.com/summoner)!  
It also acts as trusted mod repository.

All Plugins submitted will be tested and are required to be open-source, so everybody can check the functionality before running any mods themselves (theoretically).

## What was changed from the Concept:

- The High-Level-API was not build into something fully-functional. You can use it, but it just contains some helper functions, nothing to build an entire mod out of it.
- ILRepack is not needed anymore for injecting code. Having the Mod-Assembly separately has proven to be as reliable and easier.

## Developer API

First tutorial over here: http://www.scrollsguide.com/forum/viewtopic.php?f=61&t=1873

## Build instructions (needs to be updated)

### Requirements

- something that can open the project files (Xamarin Studio is free and recommended)
- the latest version of Mono (2.10.9)
- xbuild and mono need to be in your path (should be `C:\Program Files (x86)\Mono-2.10.9\bin` on windows).

### How to setup the build environment

1. Copy Assembly-CSharp.dll (from the ModLoader-Folder or an unmodified Caller's Bane), UnityEngine.dll and JsonFx.dll to the main project folder
The next step differs on MacOS and Windows:
MacOS:
  2. Open the LinFu Solution file with Xamarin Studio. Run "build all". This will build LinFu, but references won't still not work.
  Alternatively: open the terminal, `cd` into your Linfu-master folder and run `xbuild LinFu.sln`
Windows:
  2. Run `go.bat compile-release` on the `cmd` in the `LinFu-master`-Folder.
3. Restart Xamarin. The references to LinFu should now resolve correctly
4. You are now ready to build/execute the project in Xamarin.


## Future Plans

1. First major goal is to ensure that the ModLoader is fully compatible with the new version (Caller's Bane)
2. Update and maintain a repository of previous mods that are compatible with Caller's Bane
3. TBA
